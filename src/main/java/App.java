import java.util.*;

class App {
    static final int BUS_INTERVAl_MIN = 7;
    static final Map<Integer, BusStop> BUS_STOPS = new HashMap<>();
    static final Map<RouteNumber, Route> ROUTES = new HashMap<>();

    static {
        System.out.println("Init Bus stops...");

        BUS_STOPS.put(100, new BusStop(100, "Bus stop # 100"));
        BUS_STOPS.put(101, new BusStop(101, "Bus stop # 101"));
        BUS_STOPS.put(102, new BusStop(102, "Bus stop # 102"));
        BUS_STOPS.put(103, new BusStop(103, "Bus stop # 103"));
        BUS_STOPS.put(104, new BusStop(104, "Bus stop # 104"));
        BUS_STOPS.put(107, new BusStop(107, "Bus stop # 107"));
        BUS_STOPS.put(110, new BusStop(110, "Bus stop # 110"));
        BUS_STOPS.put(111, new BusStop(111, "Bus stop # 111"));
        BUS_STOPS.put(126, new BusStop(126, "Bus stop # 126"));
        BUS_STOPS.put(128, new BusStop(128, "Bus stop # 128"));
        BUS_STOPS.put(130, new BusStop(130, "Bus stop # 130"));
        BUS_STOPS.put(133, new BusStop(133, "Bus stop # 133"));
        BUS_STOPS.put(140, new BusStop(140, "Bus stop # 140"));
        BUS_STOPS.put(142, new BusStop(142, "Bus stop # 142"));
        BUS_STOPS.put(144, new BusStop(144, "Bus stop # 144"));
        BUS_STOPS.put(146, new BusStop(146, "Bus stop # 146"));
        BUS_STOPS.put(149, new BusStop(149, "Bus stop # 149"));
        BUS_STOPS.put(152, new BusStop(152, "Bus stop # 152"));
        BUS_STOPS.put(154, new BusStop(154, "Bus stop # 154"));
        BUS_STOPS.put(155, new BusStop(155, "Bus stop # 155"));
        BUS_STOPS.put(164, new BusStop(164, "Bus stop # 164"));
        BUS_STOPS.put(165, new BusStop(165, "Bus stop # 165"));
        BUS_STOPS.put(166, new BusStop(166, "Bus stop # 166"));
        BUS_STOPS.put(167, new BusStop(167, "Bus stop # 167"));
        BUS_STOPS.put(168, new BusStop(168, "Bus stop # 168"));
        BUS_STOPS.put(169, new BusStop(169, "Bus stop # 169"));
        BUS_STOPS.put(170, new BusStop(170, "Bus stop # 170"));
        BUS_STOPS.put(171, new BusStop(171, "Bus stop # 171"));
        BUS_STOPS.put(172, new BusStop(172, "Bus stop # 172"));
        BUS_STOPS.put(173, new BusStop(173, "Bus stop # 173"));
        BUS_STOPS.put(174, new BusStop(174, "Bus stop # 174"));
        BUS_STOPS.put(175, new BusStop(175, "Bus stop # 175"));
        BUS_STOPS.put(176, new BusStop(176, "Bus stop # 176"));
        BUS_STOPS.put(177, new BusStop(177, "Bus stop # 177"));
        BUS_STOPS.put(178, new BusStop(178, "Bus stop # 178"));
        BUS_STOPS.put(179, new BusStop(179, "Bus stop # 179"));
        BUS_STOPS.put(180, new BusStop(180, "Bus stop # 180"));
        BUS_STOPS.put(181, new BusStop(181, "Bus stop # 181"));
        BUS_STOPS.put(182, new BusStop(182, "Bus stop # 182"));
        BUS_STOPS.put(183, new BusStop(183, "Bus stop # 183"));
        BUS_STOPS.put(184, new BusStop(184, "Bus stop # 184"));
        BUS_STOPS.put(185, new BusStop(185, "Bus stop # 185"));
        BUS_STOPS.put(186, new BusStop(186, "Bus stop # 186"));
        BUS_STOPS.put(187, new BusStop(187, "Bus stop # 187"));
        BUS_STOPS.put(188, new BusStop(188, "Bus stop # 188"));
        BUS_STOPS.put(189, new BusStop(189, "Bus stop # 189"));
        BUS_STOPS.put(190, new BusStop(190, "Bus stop # 190"));
        BUS_STOPS.put(191, new BusStop(191, "Bus stop # 191"));
        BUS_STOPS.put(192, new BusStop(192, "Bus stop # 192"));
        BUS_STOPS.put(193, new BusStop(193, "Bus stop # 193"));
        BUS_STOPS.put(194, new BusStop(194, "Bus stop # 194"));
        BUS_STOPS.put(202, new BusStop(202, "Bus stop # 202"));
        BUS_STOPS.put(203, new BusStop(203, "Bus stop # 203"));
        BUS_STOPS.put(204, new BusStop(204, "Bus stop # 204"));
        BUS_STOPS.put(205, new BusStop(205, "Bus stop # 205"));
        BUS_STOPS.put(227, new BusStop(227, "Bus stop # 227"));
        BUS_STOPS.put(228, new BusStop(228, "Bus stop # 228"));
        BUS_STOPS.put(252, new BusStop(252, "Bus stop # 252"));
        BUS_STOPS.put(259, new BusStop(259, "Bus stop # 259"));
        BUS_STOPS.put(261, new BusStop(261, "Bus stop # 261"));
        BUS_STOPS.put(264, new BusStop(264, "Bus stop # 264"));
        BUS_STOPS.put(28327, new BusStop(301, "Bus stop # 301"));
        BUS_STOPS.put(28332, new BusStop(28332, "Bus stop # 28332"));
        BUS_STOPS.put(324, new BusStop(324, "Bus stop # 324"));
        BUS_STOPS.put(329, new BusStop(329, "Bus stop # 329"));
        BUS_STOPS.put(331, new BusStop(331, "Bus stop # 331"));
        BUS_STOPS.put(332, new BusStop(332, "Bus stop # 332"));
        BUS_STOPS.put(333, new BusStop(333, "Bus stop # 333"));
        BUS_STOPS.put(52, new BusStop(52, "Bus stop # 52"));
        BUS_STOPS.put(53, new BusStop(53, "Bus stop # 53"));
        BUS_STOPS.put(54, new BusStop(54, "Bus stop # 54"));
        BUS_STOPS.put(55, new BusStop(55, "Bus stop # 55"));
        BUS_STOPS.put(56, new BusStop(56, "Bus stop # 56"));
        BUS_STOPS.put(57, new BusStop(57, "Bus stop # 57"));
        BUS_STOPS.put(58, new BusStop(58, "Bus stop # 58"));
        BUS_STOPS.put(59, new BusStop(59, "Bus stop # 59"));
        BUS_STOPS.put(60, new BusStop(60, "Bus stop # 60"));
        BUS_STOPS.put(61, new BusStop(61, "Bus stop # 61"));
        BUS_STOPS.put(62, new BusStop(62, "Bus stop # 62"));
        BUS_STOPS.put(63, new BusStop(63, "Bus stop # 63"));
        BUS_STOPS.put(64, new BusStop(64, "Bus stop # 64"));
        BUS_STOPS.put(6475, new BusStop(6475, "Bus stop # 6475"));
        BUS_STOPS.put(6539, new BusStop(6539, "Bus stop # 6539"));
        BUS_STOPS.put(6557, new BusStop(6557, "Bus stop # 6557"));
        BUS_STOPS.put(6558, new BusStop(6558, "Bus stop # 6558"));
        BUS_STOPS.put(6559, new BusStop(6559, "Bus stop # 6559"));
        BUS_STOPS.put(66, new BusStop(66, "Bus stop # 66"));
        BUS_STOPS.put(67, new BusStop(67, "Bus stop # 67"));
        BUS_STOPS.put(68, new BusStop(68, "Bus stop # 68"));
        BUS_STOPS.put(69, new BusStop(69, "Bus stop # 69"));
        BUS_STOPS.put(70, new BusStop(70, "Bus stop # 70"));
        BUS_STOPS.put(71, new BusStop(71, "Bus stop # 71"));
        BUS_STOPS.put(72, new BusStop(72, "Bus stop # 72"));
        BUS_STOPS.put(73, new BusStop(73, "Bus stop # 73"));
        BUS_STOPS.put(74, new BusStop(74, "Bus stop # 74"));
        BUS_STOPS.put(75, new BusStop(75, "Bus stop # 75"));
        BUS_STOPS.put(77, new BusStop(77, "Bus stop # 77"));
        BUS_STOPS.put(78, new BusStop(78, "Bus stop # 78"));
        BUS_STOPS.put(84, new BusStop(84, "Bus stop # 84"));
        BUS_STOPS.put(86, new BusStop(86, "Bus stop # 86"));
        BUS_STOPS.put(87, new BusStop(87, "Bus stop # 87"));
        BUS_STOPS.put(93, new BusStop(93, "Bus stop # 93"));
        BUS_STOPS.put(94, new BusStop(94, "Bus stop # 94"));
        BUS_STOPS.put(95, new BusStop(95, "Bus stop # 95"));
        BUS_STOPS.put(96, new BusStop(96, "Bus stop # 96"));
        BUS_STOPS.put(97, new BusStop(97, "Bus stop # 97"));
        BUS_STOPS.put(9764, new BusStop(9764, "Bus stop # 9764"));
        BUS_STOPS.put(99, new BusStop(99, "Bus stop # 99"));
    }

    static {
        System.out.println("Init Routes...");

        Route route30 = route30Init();


        ROUTES.put(RouteNumber.A30, route30);
    }

    private static Route route30Init() {
        Bus directBus = new Bus(1, Direction.DIRECT, BUS_STOPS.get(182));
        Bus reverseBus = new Bus(2, Direction.REVERSE, BUS_STOPS.get(170));
        List<Bus> buses = new ArrayList<Bus>() {{
            add(directBus);
            add(reverseBus);
        }};
        Route route = new Route("Route # 30", buses);
        route.setDirect(Arrays.asList(
                BUS_STOPS.get(165), BUS_STOPS.get(172), BUS_STOPS.get(171),
                BUS_STOPS.get(168), BUS_STOPS.get(167), BUS_STOPS.get(173),
                BUS_STOPS.get(176), BUS_STOPS.get(178), BUS_STOPS.get(71),
                BUS_STOPS.get(73), BUS_STOPS.get(75), BUS_STOPS.get(78),
                BUS_STOPS.get(182), BUS_STOPS.get(184), BUS_STOPS.get(179),
                BUS_STOPS.get(185), BUS_STOPS.get(188), BUS_STOPS.get(189),
                BUS_STOPS.get(329), BUS_STOPS.get(193), BUS_STOPS.get(191),
                BUS_STOPS.get(204), BUS_STOPS.get(203), BUS_STOPS.get(301)
        ));
        route.setReverse(Arrays.asList(
                BUS_STOPS.get(164), BUS_STOPS.get(202), BUS_STOPS.get(324),
                BUS_STOPS.get(205), BUS_STOPS.get(192), BUS_STOPS.get(194),
                BUS_STOPS.get(190), BUS_STOPS.get(187), BUS_STOPS.get(186),
                BUS_STOPS.get(180), BUS_STOPS.get(6475), BUS_STOPS.get(183),
                BUS_STOPS.get(181), BUS_STOPS.get(77), BUS_STOPS.get(74),
                BUS_STOPS.get(72), BUS_STOPS.get(227), BUS_STOPS.get(177),
                BUS_STOPS.get(175), BUS_STOPS.get(174), BUS_STOPS.get(166),
                BUS_STOPS.get(169), BUS_STOPS.get(170), BUS_STOPS.get(164)
        ));
        return route;
    }

    public int calculateTripTime(Integer stopBusIdFrom, Integer stopBusIdTo) {
        return 0;
    }

}
