import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

class Route {
    private String name;
    private List<BusStop> direct = new ArrayList<>();
    private List<BusStop> reverse = new ArrayList<>();
    private List<Bus> buses = new ArrayList<>();

    Route(String name, List<Bus> buses) {
        this.name = name;
        this.buses = buses;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    List<BusStop> getDirect() {
        return direct;
    }

    void setDirect(List<BusStop> direct) {
        this.direct = direct;
    }

    public List<Bus> getBuses() {
        return buses;
    }

    public void setBuses(List<Bus> buses) {
        this.buses = buses;
    }

    List<BusStop> getReverse() {
        return reverse;
    }

    void setReverse(List<BusStop> reverse) {
        this.reverse = reverse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Route route = (Route) o;
        return  Objects.equals(name, route.name) &&
                Objects.equals(direct, route.direct) &&
                Objects.equals(reverse, route.reverse) &&
                Objects.equals(buses, route.buses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, direct, reverse, buses);
    }

    @Override
    public String toString() {
        return "Route{" +
                "name='" + name + '\'' +
                ", direct=" + direct +
                ", reverse=" + reverse +
                ", buses=" + buses +
                '}';
    }
}
