import java.util.Objects;

public class Bus {
    private int id;
    private Direction direction;
    private BusStop currentBusPosition;

    public Bus(int id, Direction direction, BusStop currentBusPosition) {
        this.id = id;
        this.direction = direction;
        this.currentBusPosition = currentBusPosition;
    }

    public Bus(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public BusStop getCurrentBusPosition() {
        return currentBusPosition;
    }

    public void setCurrentBusPosition(BusStop currentBusPosition) {
        this.currentBusPosition = currentBusPosition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bus bus = (Bus) o;
        return id == bus.id &&
                direction == bus.direction &&
                Objects.equals(currentBusPosition, bus.currentBusPosition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, direction, currentBusPosition);
    }

    @Override
    public String toString() {
        return "Bus{" +
                "id=" + id +
                ", direction=" + direction +
                ", currentBusStop=" + currentBusPosition +
                '}';
    }
}
