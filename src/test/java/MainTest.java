import org.junit.Test;

public class MainTest {


    @Test
    public void testCase1() {
        App app = new App();

        int tripTime = app.calculateTripTime(193, 167);
        assert tripTime == 154;
    }

    @Test
    public void testCase2() {
        App app = new App();

        int tripTime = app.calculateTripTime(165, 184);
        assert tripTime == 329;
    }

    @Test
    public void testCaseReverse1() {
        App app = new App();

        int tripTime = app.calculateTripTime(187, 194);
        assert tripTime == 133;
    }
}
