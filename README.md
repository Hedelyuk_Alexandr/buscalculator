

## Conditions

1. It is round bus route. 
2. Route includes 2 directions: DIRECT and REVERSE
3. Each direction contains unique bus stops.
4. On each direction can be only one bus.
5. Each bus has hardcoded position on some  bus stop.  
5. Time from one bus stop to the next is fixed (7 min).
## Task
1. To implement method 
```
public int calculateTripTime(Integer stopBusIdFrom, Integer stopBusIdTo) 
```
which calculates time of trip from bus stop A to bus stop B.


